﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WordSearch;

namespace WordSearchTest {
  [TestClass]
  public class FileContentParserTests {
    
    [TestMethod]
    public void WhenStringInput_ThenReturnWords() {
      var input = $@"AB, CDE, FGHI, JKLMN
                    Q, W, E, R, T, Y
                    A, S, D, F, G, H
                    Z, X, C, V, B, B
                    N, H, T, R, D, S
                    C, F, E, J, G, H
                    W, Q, B, T, D, F";

      var expectedResult = new List<string> { "AB", "CDE", "FGHI", "JKLMN" };

      var parser = new FileContentParser();
      var words = parser.GetWordsToLookup(input);

      CollectionAssert.AreEqual(expectedResult, words);
    }

    [TestMethod]
    public void WhenStringInput_ThenReturnWordGrid() {
      var input = $@"AB, CDE, FGHI, JKLMN
                    Q, W, E, R, T, Y
                    A, S, D, F, G, H
                    Z, X, C, V, B, B
                    N, H, T, R, D, S
                    C, F, E, J, G, H
                    W, Q, B, T, D, F";

      var parser = new FileContentParser();
      var words = parser.GetWordGrid(input);

      Assert.AreEqual(6, words.GetLength(0));
      Assert.AreEqual(6, words.GetLength(1));

      Assert.AreEqual("Q", words[0,0]);
      Assert.AreEqual("Y", words[0,5]);
      Assert.AreEqual("W", words[5,0]);
      Assert.AreEqual("F", words[5,5]);
    }
  }
}
