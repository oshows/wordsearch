using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using WordSearch;

namespace WordSearchTest {
  [TestClass]
  public class WordGridTests {
    WordGrid _wordGrid;

    [TestInitialize]
    public void Setup() {
      char[,] letters = new char[10, 10] {
          {'H','T','X','H','P','T','T','P','H','S'},
          {'T','M','L','O','S','J','Q','A','R','V'},
          {'L','K','I','D','E','R','G','X','X','N'},
          {'Y','E','H','W','M','X','P','F','N','X'},
          {'Y','G','K','K','U','C','T','L','C','L'},
          {'H','O','W','Q','R','A','H','Y','X','F'},
          {'M','W','K','T','S','W','R','I','C','E'},
          {'G','A','L','Q','L','G','S','F','S','V'},
          {'Q','Z','K','M','Q','C','N','Y','A','L'},
          {'E','N','I','P','H','A','L','Y','J','O'}
      };

      _wordGrid = new WordGrid(letters);
    }

    [TestMethod]
    [DataRow("KK", "2, 4, 3, 4")]
    [DataRow("KID", "1, 2, 2, 2, 3, 2")]
    [DataRow("LGSF", "4, 7, 5, 7, 6, 7, 7, 7")]
    [DataRow("ALYJO", "5, 9, 6, 9, 7, 9, 8, 9, 9, 9")]
    [DataRow("YGKKUC", "0, 4, 1, 4, 2, 4, 3, 4, 4, 4, 5, 4")]
    [DataRow("MLOSJQA", "1, 1, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1, 7, 1")]
    [DataRow("QZKMQCNY", "0, 8, 1, 8, 2, 8, 3, 8, 4, 8, 5, 8, 6, 8, 7, 8")]
    [DataRow("EHWMXPFNX", "1, 3, 2, 3, 3, 3, 4, 3, 5, 3, 6, 3, 7, 3, 8, 3, 9, 3")]
    [DataRow("HOWQRAHYXF", "0, 5, 1, 5, 2, 5, 3, 5, 4, 5, 5, 5, 6, 5, 7, 5, 8, 5, 9, 5")]
    public void WhenISearchForAWordThatExistsHorizontally_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }


    [TestMethod]
    [DataRow("AZ", "1, 7, 1, 8")]
    [DataRow("PAX", "7, 0, 7, 1, 7, 2")]
    [DataRow("FEVL", "9, 5, 9, 6, 9, 7, 9, 8")]
    [DataRow("YIFYY", "7, 5, 7, 6, 7, 7, 7, 8, 7, 9")]
    [DataRow("XLIHKW", "2, 0, 2, 1, 2, 2, 2, 3, 2, 4, 2, 5")]
    [DataRow("GPTHRSN", "6, 2, 6, 3, 6, 4, 6, 5, 6, 6, 6, 7, 6, 8")]
    [DataRow("HTLYYHMG", "0, 0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7")]
    [DataRow("ODWKQTQMP", "3, 1, 3, 2, 3, 3, 3, 4, 3, 5, 3, 6, 3, 7, 3, 8, 3, 9")]
    [DataRow("PSEMURSLQH", "4, 0, 4, 1, 4, 2, 4, 3, 4, 4, 4, 5, 4, 6, 4, 7, 4, 8, 4, 9")]
    public void WhenISearchForAWordThatExistsVertically_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }

    [TestMethod]
    [DataRow("YO", "0, 4, 1, 5")]
    [DataRow("HIS", "6, 5, 7, 6, 8, 7")]
    [DataRow("TLDM", "1, 0, 2, 1, 3, 2, 4, 3")]
    [DataRow("RPLXE", "5, 2, 6, 3, 7, 4, 8, 5, 9, 6")]
    [DataRow("KHKRWSY", "1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8")]
    [DataRow("LDMCHISL", "2, 1, 3, 2, 4, 3, 5, 4, 6, 5, 7, 6, 8, 7, 9, 8")]
    [DataRow("HMIWUARFAO", "0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9")]
    public void WhenISearchForAWordThatExistsDiagonallyDescending_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }

    [TestMethod]
    [DataRow("IX", "7, 6, 8, 5")]
    [DataRow("QUX", "3, 5, 4, 4, 5, 3")]
    [DataRow("WHLN", "5, 6, 6, 5, 7, 4, 8, 3")]
    [DataRow("IMLWH", "2, 9, 3, 8, 4, 7, 5, 6, 6, 5")]
    [DataRow("MLWHLNN", "3, 8, 4, 7, 5, 6, 6, 5, 7, 4, 8, 3, 9, 2")]
    [DataRow("AKQUXGAH", "1, 7, 2, 6, 3, 5, 4, 4, 5, 3, 6, 2, 7, 1, 8, 0")]
    [DataRow("EZLTRCPXRS", "0, 9, 1, 8, 2, 7, 3, 6, 4, 5, 5, 4, 6, 3, 7, 2, 8, 1, 9, 0")]
    public void WhenISearchForAWordThatExistsDiagonallyAscending_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }


    [TestMethod]
    [DataRow("QJ", "6, 1, 5, 1")]
    [DataRow("NFP", "8, 3, 7, 3, 6, 3")]
    [DataRow("STKW", "4, 6, 3, 6, 2, 6, 1, 6")]
    [DataRow("AQJSO", "7, 1, 6, 1, 5, 1, 4, 1, 3, 1")]
    [DataRow("OJYLAH", "9, 9, 8, 9, 7, 9, 6, 9, 5, 9, 4, 9")]
    [DataRow("GREDIKL", "6, 2, 5, 2, 4, 2, 3, 2, 2, 2, 1, 2, 0, 2")]
    [DataRow("AYNCQMKZ", "8, 8, 7, 8, 6, 8, 5, 8, 4, 8, 3, 8, 2, 8, 1, 8")]
    [DataRow("CLTCUKKGY", "8, 4, 7, 4, 6, 4, 5, 4, 4, 4, 3, 4, 2, 4, 1, 4, 0, 4")]
    [DataRow("FXYHARQWOH", "9, 5, 8, 5, 7, 5, 6, 5, 5, 5, 4, 5, 3, 5, 2, 5, 1, 5, 0, 5")]
    public void WhenISearchForAWordThatExistsHorizontallyReversed_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }

    [TestMethod]
    [DataRow("ZA", "1, 8, 1, 7")]
    [DataRow("RUM", "4, 5, 4, 4, 4, 3")]
    [DataRow("JASC", "8, 9, 8, 8, 8, 7, 8, 6")]
    [DataRow("LFXAP", "7, 4, 7, 3, 7, 2, 7, 1, 7, 0")]
    [DataRow("GMHYYL", "0, 7, 0, 6, 0, 5, 0, 4, 0, 3, 0, 2")]
    [DataRow("MQTQKWD", "3, 8, 3, 7, 3, 6, 3, 5, 3, 4, 3, 3, 3, 2")]
    [DataRow("IKLKWKHI", "2, 9, 2, 8, 2, 7, 2, 6, 2, 5, 2, 4, 2, 3, 2, 2")]
    [DataRow("LVEFLXNVS", "9, 8, 9, 7, 9, 6, 9, 5, 9, 4, 9, 3, 9, 2, 9, 1, 9, 0")]
    [DataRow("ACGWACXRJT", "5, 9, 5, 8, 5, 7, 5, 6, 5, 5, 5, 4, 5, 3, 5, 2, 5, 1, 5, 0")]
    public void WhenISearchForAWordThatExistsVerticallyReversed_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }

    [TestMethod]
    [DataRow("CM", "5, 4, 4, 3")]
    [DataRow("IZG", "2, 9, 1, 8, 0, 7")]
    [DataRow("QQKO", "4, 8, 3, 7, 2, 6, 1, 5")]
    [DataRow("VCYTX", "9, 7, 8, 6, 7, 5, 6, 4, 5, 3")]
    [DataRow("JYSWRK", "8, 9, 7, 8, 6, 7, 5, 6, 4, 5, 3, 4")]
    [DataRow("LSIHCMDLT", "9, 8, 8, 7, 7, 6, 6, 5, 5, 4, 4, 3, 3, 2, 2, 1, 1, 0")]
    [DataRow("OAFRAUWIMH", "9, 9, 8, 8, 7, 7, 6, 6, 5, 5, 4, 4, 3, 3, 2, 2, 1, 1, 0, 0")]
    public void WhenISearchForAWordThatExistsDiagonallyDescendingReversed_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }

    [TestMethod]
    [DataRow("JE", "5, 1, 4, 2")]
    [DataRow("TAS", "6, 4, 5, 5, 4, 6")]
    [DataRow("OIEY", "3, 1, 2, 2, 1, 3, 0, 4")]
    [DataRow("NNLHW", "9, 2, 8, 3, 7, 4, 6, 5, 5, 6")]
    [DataRow("GXUQKA", "6, 2, 5, 3, 4, 4, 3, 5, 2, 6, 1, 7")]
    [DataRow("HAGXUQKAQ", "8, 0, 7, 1, 6, 2, 5, 3, 4, 4, 3, 5, 2, 6, 1, 7, 0, 8")]
    [DataRow("SRXPCRTLZE", "9, 0, 8, 1, 7, 2, 6, 3, 5, 4, 4, 5, 3, 6, 2, 7, 1, 8, 0, 9")]
    public void WhenISearchForAWordThatExistsDiagonallyAscendingReversed_ThenReturnTheCorrectCoordinates(string word, string expectedResultedAsString) {
      var expectedResult = expectedResultedAsString.Split(',').Select(int.Parse).ToList();
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(expectedResult, result);
    }


    [TestMethod]
    [DataRow("ZX")]
    [DataRow("BBB")]
    [DataRow("PFNC")]
    [DataRow("SFSVQ")]
    [DataRow("PINELA")]
    [DataRow("FELVOJY")]
    [DataRow("PTTPHSHT")]
    public void WhenISearchForAWordThatDoesNotExist_ThenReturnNoCoordinates(string word) {
      List<int> result = _wordGrid.Find(word);

      CollectionAssert.AreEqual(new List<int>(), result);
    }
  }
}