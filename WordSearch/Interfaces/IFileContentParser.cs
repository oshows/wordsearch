﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSearch.Interfaces {
  interface IFileContentParser {
    List<string> GetWordsToLookup(string input);
    string[,] GetWordGrid(string input);

  }
}
