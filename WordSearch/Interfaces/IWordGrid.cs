﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSearch.Interfaces {
  interface IWordGrid {
    List<int> Find(string word);

  }
}
