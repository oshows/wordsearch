﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WordSearch {
  public class FileContentParser {
    public List<string> GetWordsToLookup(string input) {
      using (var reader = new StringReader(input)) {
        var wordString = reader.ReadLine();
        var words = wordString.Split(',').Select(e => e.Trim());
        return words.ToList();
      }
    }

    public string[,] GetWordGrid(string input) {
      var reader = new StringReader(input);
      var wordString = reader.ReadLine();

      List<string[]> resultList = new List<string[]>();

      while ((wordString = reader.ReadLine()) != null) {
        resultList.Add(wordString.Split(',').Select(e => e.Trim()).ToArray());
      }

      string[,] result = new string[resultList.Count, resultList.Count];
      for (int i = 0; i < result.GetLength(0); i++) {
        for (int j = 0; j < result.GetLength(1); j++) {
          result[i, j] = resultList[i][j];
        }
      }
      return result;
    }
  }
}
