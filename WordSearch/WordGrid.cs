﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WordSearch.Interfaces;

namespace WordSearch {
  public class WordGrid : IWordGrid{
    private char[,] _letters;

    public WordGrid(char[,] letters) {
      _letters = letters;
    }

    public List<int> Find(string word) {
      var builder = new StringBuilder();

      var coordinates = FindHorizontally(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindVertically(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindDiagonallyDescending(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindDiagonallyAscending(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindHorizontallyReversed(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindVerticallyReversed(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindDiagonallyDescendingReversed(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      coordinates = FindDiagonallyAscendingReversed(word);
      if (coordinates.Any()) {
        return coordinates;
      }

      return new List<int>();
    }

    private List<int> FindHorizontally(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();
      for (int i = 0; i < _letters.GetLength(0); i++) {
        for (int j = 0; j <= _letters.GetLength(1) - word.Length; j++) {
          builder.Clear();
          coordinates.Clear();

          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i, j + k]);
            coordinates.Add(j + k);
            coordinates.Add(i);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }
        }
      }
      return new List<int>();
    }

    private List<int> FindVertically(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();

      for (int i = 0; i <= _letters.GetLength(0) - word.Length; i++) {
        for (int j = 0; j < _letters.GetLength(1); j++) {
          builder.Clear();
          coordinates.Clear();

          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i + k, j]);
            coordinates.Add(j);
            coordinates.Add(i + k);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }

        }
      }
      return new List<int>();
    }

    private List<int> FindDiagonallyDescending(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();

      for (int i = 0; i <= _letters.GetLength(0) - word.Length; i++) {
        for (int j = 0; j <= _letters.GetLength(1) - word.Length; j++) {
          builder.Clear();
          coordinates.Clear();

          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i + k, j + k]);
            coordinates.Add(j + k);
            coordinates.Add(i + k);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }
        }
      }
      return new List<int>();
    }

    private List<int> FindDiagonallyAscending(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();
      for (int i = word.Length - 1; i < _letters.GetLength(0); i++) {
        for (int j = 0; j <= _letters.GetLength(1) - word.Length; j++) {
          builder.Clear();
          coordinates.Clear();
          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i - k, j + k]);
            coordinates.Add(j + k);
            coordinates.Add(i - k);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }

        }
      }
      return new List<int>();
    }


    private List<int> FindHorizontallyReversed(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();

      for (int i = 0; i < _letters.GetLength(0); i++) {
        for (int j = word.Length - 1; j < _letters.GetLength(1); j++) {
          builder.Clear();
          coordinates.Clear();
          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i, j - k]);
            coordinates.Add(j - k);
            coordinates.Add(i);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }
        }
      }
      return new List<int>();
    }

    private List<int> FindVerticallyReversed(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();
      for (int i = word.Length - 1; i < _letters.GetLength(0); i++) {
        for (int j = 0; j < _letters.GetLength(1); j++) {
          builder.Clear();
          coordinates.Clear();
          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i - k, j]);
            coordinates.Add(j);
            coordinates.Add(i - k);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }
        }
      }
      return new List<int>();
    }

    private List<int> FindDiagonallyDescendingReversed(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();
      for (int i = word.Length - 1; i < _letters.GetLength(0); i++) {
        for (int j = word.Length - 1; j < _letters.GetLength(1); j++) {
          builder.Clear();
          coordinates.Clear();
          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i - k, j - k]);
            coordinates.Add(j - k);
            coordinates.Add(i - k);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }
        }
      }
      return new List<int>();
    }


    private List<int> FindDiagonallyAscendingReversed(string word) {
      var builder = new StringBuilder();
      var coordinates = new List<int>();
      for (int i = 0; i <= _letters.GetLength(0) - word.Length; i++) {
        for (int j = word.Length - 1; j < _letters.GetLength(1); j++) {
          builder.Clear();
          coordinates.Clear();
          for (int k = 0; k < word.Length; k++) {
            builder.Append(_letters[i + k, j - k]);
            coordinates.Add(j - k);
            coordinates.Add(i + k);
          }

          if (builder.ToString() == word) {
            return coordinates;
          }
        }
      }
      return new List<int>();
    }


  }


}
